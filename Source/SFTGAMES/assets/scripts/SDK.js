window._Game = {};
_Game.Account = {
    Name : 'Test',
    Level : 1,
    Exp : 0,
    Base : {
        HP : 50,
        Atk : 5,
        Def : 0,
        Crit : 0,
        Hit : 0,
        Spd : 0
    },
    Levelup : {
        HP : 12,
        Atk : 1,
        Def : 1,
        Crit : 0,
        Hit : 2,
        Spd : 0,
        Exp : 100
    }
};
_Game.monsterPrefabs = {};
_Game.curLevel = 3;
_Game.level = [
    {
        level : 1,
        monsters : ['M1'],
        rounds : [
            [
                {name : 'M1', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ]
        ],
        items : [
            {name : 'gold' , amount : 1}
        ] 
    },
    {
        level : 2,
        monsters : ['M1','M1','M2'],
        rounds : [
            [
                {name : 'M1', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M1', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {name : 'gold' , amount : 1}
        ] 
    },
    {
        level : 3,
        monsters : ['M1','M2','B1'],
        rounds : [
            [
                {name : 'M1', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'B1', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {id : 'S001',name : 'sword' , },
            {id : 'G001',name : 'gold' , amount : 2}
        ] 
    },
    {
        level : 4,
        monsters : ['M2','M2','M3'],
        rounds : [
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {id : 'G001',name : 'gold' , amount : 1}
        ] 
    },
    {
        level : 5,
        monsters : ['M2','M3','M3'],
        rounds : [
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {id : 'G001',name : 'gold' , amount : 1}
        ] 
    },
    {
        level : 6,
        monsters : ['M2','M3','B2'],
        rounds : [
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'B2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {id : 'A001',name : 'armo' , },
            {id : 'G001',name : 'gold' , amount : 3}
        ] 
    },
    {
        level : 7,
        monsters : ['M1','M2'],
        rounds : [
            [
                {name : 'M1', HP : 50 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M2', HP : 50 , Atk : 3 , Def : 0 , Crit : 0}
            ]
        ],
        items : [
            {id : 'G001',name : 'gold' , amount : 1}
        ] 
    },
    {
        level : 8,
        monsters : ['M2','M3','M3'],
        rounds : [
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {id : 'G001',name : 'gold' , amount : 1}
        ] 
    },
    {
        level : 9,
        monsters : ['M3','M3','M3'],
        rounds : [
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {id : 'G001',name : 'gold' , amount : 1}
        ] 
    },
    {
        level : 10,
        monsters : ['M3','M3','B3'],
        rounds : [
            [
                {name : 'M2', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'M3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
            [
                {name : 'B3', HP : 30 , Atk : 3 , Def : 0 , Crit : 0}
            ],
        ],
        items : [
            {id : 'N001',name : 'Neck' , },
            {id : 'G001',name : 'gold' , amount : 1}
        ] 
    },
];