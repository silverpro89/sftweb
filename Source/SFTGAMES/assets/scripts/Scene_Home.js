// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        UserInfoPrefab : cc.Prefab
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var self = this;
        self.ControllerUserInfo = null;
        window._controller = self;
    },

    start () {
        var self = this;
        self.loadUserInfo();
        var _onSelect = function(){
            cc.director.loadScene('Scene_SelectLevel');
        };
        cc.find("Canvas/Layout/layer_main/Menu/Adventure").on(cc.Node.EventType.TOUCH_START,_onSelect);

        var _onRanking = function(){
            cc.director.loadScene('Scene_Ranking');
        };
        cc.find("Canvas/Layout/layer_main/Menu/Ranking").on(cc.Node.EventType.TOUCH_START,_onRanking);
        
        var _onHover = function(){
            
        };
        cc.find("Canvas/Layout/layer_main/Menu/Adventure").on(cc.Node.EventType.MOUSE_MOVE,_onHover);
        cc.find("Canvas/Layout/layer_main/Menu/Adventure").on(cc.Node.EventType.TOUCH_MOVE,_onHover);
    },

    loadUserInfo (){
        var self = this;
        var _tmp = cc.instantiate(self.UserInfoPrefab);
        self.ControllerUserInfo = _tmp.getComponent("HUD_character");
        _tmp.name = 'UserInfoNode';
        _tmp.x = -500;
        _tmp.y = 250;
        cc.find("Canvas/Layout/layer_top").addChild(_tmp);
    },

    loadCharacterPopup (){
        var self = this;
        var _tmp = cc.instantiate(self.UserInfoPrefab);
        self.ControllerUserInfo = _tmp.getComponent("HUD_character");
        _tmp.name = 'UserInfoNode';
        _tmp.x = -500;
        _tmp.y = 250;
        cc.find("Canvas/Layout/layer_top").addChild(_tmp);
        if (_callback) _callback();
    },


    // update (dt) {},
});
