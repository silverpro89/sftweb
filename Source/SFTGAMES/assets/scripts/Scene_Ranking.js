// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
    },

    start () {
        var _onSelect = function(){
            cc.director.loadScene('Scene_Home');
        };
        cc.find("Canvas/Layout/back/btn_back").on(cc.Node.EventType.TOUCH_START,_onSelect);
        var _onHover = function(){
            
        };
        cc.find("Canvas/Layout/layer_main/Menu/Adventure").on(cc.Node.EventType.MOUSE_MOVE,_onHover);
        cc.find("Canvas/Layout/layer_main/Menu/Adventure").on(cc.Node.EventType.TOUCH_MOVE,_onHover);
    },

    // update (dt) {},
});
