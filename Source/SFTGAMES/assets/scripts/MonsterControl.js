// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var self = this;
        self.currentAnim = "";
        self.AnimCompleteCallback = null;
        self.AnimStartCallback = null;
    },

    start () {

    },
    move (x , callback){
        var self = this,
        _heroAnim = null, 
        _distance = Math.abs(this.node.x - x),
        _time = _distance / 150;
        _heroAnim = this.node.getChildByName('Anim').getComponent(sp.Skeleton);
        self.callback = null;
        _heroAnim.setAnimation(0, "Run", true);
        var _onComplete = function(){
            _heroAnim.setAnimation(0, "Idle", true);
            if (callback) callback();
        };
        cc.tween(this.node).to(_time, { x: x })
        .call(_onComplete)
        .start();
        self.AnimStartCallback = null;
        self.AnimCompleteCallback = null;
    },
    atk (_callback){
        var self = this,
        _heroAnim = null,
        _heroAnim = this.node.getChildByName('Anim').getComponent(sp.Skeleton);
        _heroAnim.setAnimation(0, "Attack", false);
        var _onComplete = function(){
            _heroAnim.setAnimation(0, "Idle", true);
            if (_callback) _callback();
        };
        setTimeout(_onComplete,1000);
    },
    hit (_damage,_callback){
        var self = this,
        _heroAnim = null,
        _heroAnim = this.node.getChildByName('Anim').getComponent(sp.Skeleton);
        _heroAnim.setAnimation(0, "Hit", false);
        self.CurHP -= _damage;
        var _onComplete = function(){
            _heroAnim.setAnimation(0, "Idle", true);
            if (self.CurHP <= 0){
                if (_callback) _callback();
            }
        };
        setTimeout(_onComplete,1000);
        self.updateHP();
    },
    die (_callback){
        var self = this,
        _heroAnim = null,
        _heroAnim = this.node.getChildByName('Anim').getComponent(sp.Skeleton);
        _heroAnim.setAnimation(0, "Die", false);
        self.scheduleOnce(function() {
            self.unschedule();
            self.node.destroy();
        }, 0.5);
    },
    loadMonster (_obj , _callback){
        var self = this;
        var _anim = _obj.name;
        console.log(_obj);
        console.log("Anim " + _anim);
        self.Monster = _obj;
        self.Atk = _obj.Atk;
        self.HP = _obj.HP;
        self.Def = _obj.Def;
        self.Crit = _obj.Crit;
        self.CurHP = _obj.HP;

        //Update HP
        self.updateHP();
        var _drawAnim = function(){
            var _tmp = cc.instantiate(_Game.monsterPrefabs[_anim]);
            _tmp.name = 'Anim';
            self.node.addChild(_tmp);
            if (_callback) _callback();
        };
        if (typeof _Game.monsterPrefabs[_anim] != "undefined"){
            _drawAnim();
        } else {
            cc.loader.loadRes(_anim, function (err, prefab) {
                _Game.monsterPrefabs[_anim] = prefab;
                _drawAnim();
            });
        }
    },
    updateHP (){
        var self = this;
        if (self.CurHP <= 0) {
            self.CurHP == 0;
            self.die();
        }
        self.node.getChildByName("top_bar").getChildByName("hp").getChildByName("HPbar").width = self.CurHP/self.HP * 100;
        self.node.getChildByName("top_bar").getChildByName("hp").getChildByName("label").getComponent('cc.Label').string = self.CurHP + ' / ' + self.HP;
    },

    // update (dt) {},
});
