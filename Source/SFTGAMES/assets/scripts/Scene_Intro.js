// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var self = this;
        self.loadingPopup = cc.find("Canvas/Layout").getChildByName('Loading');
		self.btnConnect = cc.find("Canvas/Layout").getChildByName('btn_connect');
        self.btnConnect.on(cc.Node.EventType.TOUCH_START, function () {
			self.onConnectWallet();
		});
    },

    start () {

    },

    onConnectWallet(){
        var self = this;
        var _onComplete = function(){
            cc.find("Canvas/Layout").getChildByName('Loading').active = true;
        };
        cc.tween(cc.find("Canvas/Layout").getChildByName('btn_connect'))
        .to(0.5, { y: -500 })
        .call(_onComplete)
        .start();
        self.onConnectWalletComplete();
        //_SDK.init(self.onConnectWalletComplete,self.onConnectWalletFail)
    },

    onConnectWalletComplete(){
        cc.director.loadScene('Scene_Loading');
    },

    onConnectWalletFail(){
        var self = this;
        var _onComplete = function(){
            cc.find("Canvas/Layout").getChildByName('Loading').active = false;
        };
        cc.tween(cc.find("Canvas/Layout").getChildByName('btn_connect'))
        .to(0.5, { y: -200 })
        .call(_onComplete)
        .start();
    },

    // update (dt) {},
});
