// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },
    move (x){
        var _heroAnim = null, 
        _time = _time = x / 300;
        _heroAnim = this.node.getChildByName('Anim').getComponent(sp.Skeleton);
        _heroAnim.setAnimation(0, "Run", true);
        var _onComplete = function(){
            _heroAnim.setAnimation(0, "Idle", true);
        };
        cc.tween(this.node).to(_time, { x: x })
        .call(_onComplete)
        .start();
    }
    // update (dt) {},
});
