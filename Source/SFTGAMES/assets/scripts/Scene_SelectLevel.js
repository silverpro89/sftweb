// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        monsterIcon : cc.Prefab,
        itemIcon : cc.Prefab
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var self = this;
        var _scene01 = cc.find("Canvas/Layout/scrollView/view/content/scene_01");
        for (var i = 1; i < 21 ;i++){
            var _onSelect = function(_event){
                self.showLevelInfo(_event.currentTarget.numLevel);
            };
            _scene01.getChildByName('btn_lv'+i).numLevel = i;
            _scene01.getChildByName('btn_lv'+i).on(cc.Node.EventType.TOUCH_START,_onSelect);
        }
        //
        var _onPlay = function(_event){
            cc.director.loadScene('Scene_Game');
        };
        cc.find("Canvas/Layout/popup/btn_play").on(cc.Node.EventType.TOUCH_START,_onPlay);

        var _onBack = function(_event){
            cc.director.loadScene('Scene_Home');
        };
        cc.find("Canvas/Layout/HUD/btn_back").on(cc.Node.EventType.TOUCH_START,_onBack);
    },

    start () {

    },
    showLevelInfo (level){
        var _data = _Game.level,
        self = this,
        _levelSelected = null,
        _popup = cc.find("Canvas/Layout/popup");
        _popup.y = 600;
        
        for (var i in _data){
            if (_data[i].level == level)
            _levelSelected = _data[i];
        }
        console.log(_levelSelected);
        if (_levelSelected == null) return;
        _Game.curLevel = parseInt(_levelSelected.level);
        // Load stage number
        _popup.getChildByName('stage').getComponent('cc.Label').string = "Stage "+level;
        // Load monster info
        var _monsterList = _popup.getChildByName('monsters').getChildByName('list');
        _monsterList.removeAllChildren(true);
        for (var j in _levelSelected.monsters){
            var _tmp = cc.instantiate(self.monsterIcon),
			_comTmp = _tmp.getComponent('monsterIcon');
            _tmp.width = 50;
            _tmp.height = 50;
            _comTmp.loadMonster(_levelSelected.monsters[j]);
            _monsterList.addChild(_tmp);
        }
        cc.tween(_popup).to(0.3, { y: 0 }).call().start();
    }
    // update (dt) {},
});
