// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        monsterPrefab : cc.Prefab,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        var self = this;
        self.atkTurn = "Hero";
        self.monsters = null;
        self.roundTurn = -1;
        self.totalRound = 0;
        self.isCompleteTurn = false;
    },

    start () {
        //this.heroMove();
        var self = this;
        cc.find("Canvas/Layout/Result").active = false;
        //
        cc.find("Canvas/Layout/Result/btn_home").on(cc.Node.EventType.TOUCH_START,function(){
            cc.director.loadScene('Scene_SelectLevel');
        });
        // LOAD HERO
        cc.find("Canvas/Layout/Hero").getComponent("HeroControl").onloadHeroInfo();
        // LOAD MONSTER DATA
        for (var i in _Game.level){
            if( _Game.level[i].level == _Game.curLevel )
            self.monsters = _Game.level[i].rounds;
        }
        console.log(self.monsters);
        self.totalRound = self.monsters.length;
        
        self.scheduleOnce(function() {
            self.heroMove();
        }, 1);
    },

    createMonsterRound (){
        var self = this;
        var _roundMonster = self.monsters[self.roundTurn];
        for (var j in _roundMonster){
            self.createMonster(_roundMonster[j]);    
        }
    },

    createMonster (_obj){
        var self = this;
        var _tmp = cc.instantiate(self.monsterPrefab);
        _tmp.name = 'Monster';
        _tmp.x = 740;
        _tmp.y = -135;
        _tmp.getComponent('MonsterControl').loadMonster(_obj);
        cc.find("Canvas/Layout/Monsters").addChild(_tmp);
        self.scheduleOnce(function() {
            self.monsterMove();
        }, 1);
    },

    heroMove (){
        var self = this,
        _heroControl = cc.find("Canvas/Layout/Hero").getComponent("HeroControl");
        var _onComplete = function(){
            //self.heroAttack();
            self.moveRound();
        }
        _heroControl.move(0 , _onComplete);
        
    },

    monsterMove (){
        var self = this;
        var _monsList = cc.find("Canvas/Layout/Monsters").children;
        for (var i in _monsList){
            var _tmpMonster = _monsList[i].getComponent('MonsterControl');
            var _onComplete = function(){
                self.monsterAttack();
            };
            _tmpMonster.move(100 , _onComplete);
        }
    },
    changeTurn (){
        var self = this;
        if (!self.isCompleteTurn){
            switch(self.atkTurn){
                case 'Hero' :
                    self.monsterAttack();
                break;
                case 'Monster' :
                    self.heroAttack();
                break;
            }
        } else {
            self.moveRound();
        }
    },
    heroAttack (){
        var self = this;
        self.atkTurn = 'Hero';
        var _onComplete = function(){
            self.scheduleOnce(function() {
                self.unschedule();
                self.changeTurn();
            }, 1);
        };
        cc.find("Canvas/Layout/Hero").getComponent("HeroControl").atk(_onComplete);
        self.scheduleOnce(function() {
            self.unschedule();
            var _monsList = cc.find("Canvas/Layout/Monsters").children;
            var _onMonsterDie = function(){
                self.isCompleteTurn = true;
            };
            for (var i in _monsList){
                _monsList[i].getComponent("MonsterControl").hit(10 , _onMonsterDie); 
            }
        }, 0.5);
    },
    monsterAttack (){
        var self = this;
        self.atkTurn = 'Monster';
        var _count = 0;
        var _monsList = cc.find("Canvas/Layout/Monsters").children;
        var _onComplete = function(){
            _count += 1;
            if (_count >= _monsList.length)
            self.scheduleOnce(function() {
                self.unschedule();
                self.changeTurn();
            }, 1);
        };
        var _damage = 0;
        for (var i in _monsList){
            _monsList[i].getComponent("MonsterControl").atk(_onComplete);
            _damage = _monsList[i].getComponent("MonsterControl").Atk;
        }
        
        self.scheduleOnce(function() {
            self.unschedule();
            cc.find("Canvas/Layout/Hero").getComponent("HeroControl").hit(_damage);
        }, 0.5);
    },
    update (dt) {
        var _bg = cc.find("Canvas/Layout/bg");
        //_bg.x -= 150*dt;
        //if (_bg.x <= -1436)
        //_bg.x = 300;
    },
    moveRound (){
        var self = this;
        self.roundTurn += 1;
        
        self.isCompleteTurn = false;
        var _heroControl = cc.find("Canvas/Layout/Hero").getComponent("HeroControl");
        _heroControl.run();
        var _onComplete = function(){
            _heroControl.idle();
            if (self.roundTurn == self.totalRound){
                cc.find("Canvas/Layout/Result").active = true;
                //cc.director.loadScene('Scene_SelectLevel');
            }
            else {
                self.createMonsterRound();
            }
            cc.find("Canvas/Layout/bg").x = 0;
        };
        cc.tween(cc.find("Canvas/Layout/bg")).to(5, { x: -1836 })
        .call(_onComplete)
        .start();
    },
});
