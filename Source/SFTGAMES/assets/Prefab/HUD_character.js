// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        var self = this;
        self.maxStamina = 120;
        self.maxExp = 100;
        self.obj = {
            Name : 'Test',
            Level : 1,
            Exp : 15,
            Sta : 10
        };
        self.updateAll();
    },

    updateAll(){
        var self = this;
        self.updateName();
        self.updateExp();
        self.updateSta();
    },

    updateName (){
        var self = this;
        self.node.getChildByName('Name').getComponent('cc.Label').string = "LV. "+self.obj.Level+" "+self.obj.Name;
    },
    updateExp (){
        var self = this;
        var _parent = this.node.getChildByName('Exp_bar');
        var _value = self.obj.Exp;
        var _max = self.maxExp;
        if (self.obj.Exp > 100) {
            self.obj.Exp -= 100;
            self.obj.Level += 1;
        }
        _parent.getChildByName('value').getComponent('cc.Label').string = _value +"/"+ _max;
        _parent.getChildByName('HPbar').width = (_value/100)*140;
    },
    updateSta (){
        var self = this;
        var _parent = this.node.getChildByName('Stamina');
        var _value = self.obj.Sta;
        var _max = self.maxStamina;
        _parent.getChildByName('value').getComponent('cc.Label').string = _value +"/"+ _max;
        _parent.getChildByName('HPbar').width = (_value/100)*140;
    },
    // update (dt) {},
});
