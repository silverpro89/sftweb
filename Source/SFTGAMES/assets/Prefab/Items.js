// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    loadInfo (){
        var _obj = {
            id : 1,
            type : 'Sword',
            name : 'Long Sword',
            info : {
                HP : 12,
                Atk : 1,
                Def : 1,
                Crit : 0,
                Hit : 2,
                Spd : 0
            },
            isEquip : false,
            Grade : 'White'
        };
        var _parent = this.node;
        switch (_obj.type){
            case 'Sword' : 
            break;
            case 'Ring' : 
            break;
            case 'Pendant' : 
            break;
            case 'Character' : 
            break;
            case 'Belt' : 
            break;
        }
        _parent.getChildByName('ratity').active = false;
        _parent.getChildByName('ratity').getChid = false;
    }

    // update (dt) {},
});
